<?php
session_start();
require_once("../Classes/ClientDeleteUser.php");
require_once("../Classes/GetClientRegisterData.php");
require_once("../Classes/ClientRegister.php");
(new GetClientRegisterData());

if (isset($_POST['del-btn'])) {
	(new ClientDeleteUser())->delUser();
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../css/style.css">
	<title>CrudDocker</title>
</head>
<body>

<header class="main-header">
	<h1>Hotel 100 Estrelas</h1>

	<div class="user-area">
		<div class="dropdown">
			<span> <?php echo "usuario";?> </span>
			<div class="dropdown-content">
				<p>
					<a href="update/index.php">Atualizar</a>
				</p>
				<p>
					<a href="logout/logout.php">Sair</a>
				</p>
				<p>
					<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
						<button name="del-btn">Apagar Conta</button>
					</form>
				</p>
			</div>
		</div>
	</div>
	</header>

	<button>asd</button>
</body>
</html>