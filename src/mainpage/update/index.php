<?php 
	require_once("../../Classes/scriptsPHP/queryBase.php");
	require_once("../../Classes/ClientUpdateData.php");
	
	if(isset($_POST['update-btn'])){
		(new ClientUpdateData())->updateData();
	}
 ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="../../css/style.css">
	<title>Atualizar conta</title>
</head>
<body>

	<header class="main-header">
		<h1>Hotel 100 Estrelas</h1>
	</header>
	<main>
		<form action="<?php $_SERVER['PHP_SELF'] ?>" method="POST">
			<div>
				<input type="text" name="newmail" placeholder="Novo email" name="">
			</div>
			<div>
				<input type="password" name="newpass" placeholder="Nova senha" name="">
			</div>
			<div>
				<button name="update-btn">Atualizar</button>
			</div>
		</form>
	</main>
</body>
</html>