<?php
class Conexao{
		
	private $con;

	public function getCon(){
		return $this->con;
	}

	public function setCon($data){
		$this->con = $data;
	}


	public function __construct(){
		try{
			$host    = "datahls"; // nome do container mysql
			$dbname = "hotel";
			$user = "root";
			$password = "123";
			$this->setCon(new PDO("mysql:host={$host};dbname={$dbname}", $user, $password));
			$this->con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

		}catch(PDOException $ex){
			echo "{$ex->getMessage()}";
		}
	}
}
?>