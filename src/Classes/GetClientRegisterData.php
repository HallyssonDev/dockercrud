<?php 
// classe que recebe os valores de cada input

session_start();

class GetClientRegisterData
{
	public static $g_name;
	public static $g_email;
	private static $g_cpf;
	private static $g_phone;
	private static $g_pass;
	private static $g_cpass;

	function __construct()
	{
		self::$g_name = $_POST['clientname'];
		self::$g_email = $_POST['clientmail'];
		self::setCpf($_POST['clientcpf']);
		self::setPhone($_POST['clientphone']);
		self::setPass($_POST['clientpass']);
		self::setCPass($_POST['clientcpass']);
	}

	public static function getCpf(){
		return self::$g_cpf;
	}

	public static function getPhone(){
		return self::$g_phone;
	}

	public static function setCpf($cpf){
		self::$g_cpf = $cpf;
	}

	public static function setPhone($phone){
		self::$g_phone = $phone;
	}

	public static function setPass($pass){
		self::$g_pass = $pass;
	}

	public static function getPass(){
		return self::$g_pass;
	}

	public static function setCPass($cpass){
		self::$g_cpass = $cpass;
	}

	public static function getCPass(){
		return self::$g_cpass;
	}

}
?>