<?php 

class GetClientLoginData 
{
	private static $login_mail;
	private static $login_pwd;

	function __construct()
	{
		self::setLoginMail($_POST['mailLogin']);
		self::setLoginPwd($_POST['pwdLogin']);
	}

	public static function setLoginMail($mail){
		self::$login_mail = $mail;
	}

	public static function getLoginMail(){
		return self::$login_mail;
	}

	public static function setLoginPwd($pwd){
		self::$login_pwd = $pwd;
	}

	public static function getLoginPwd(){
		return self::$login_pwd;
	}
}

?>