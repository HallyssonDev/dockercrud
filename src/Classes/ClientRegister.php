<?php 
require_once ('GetClientRegisterData.php');
require_once ('Conexao.php');
require_once ('scriptsPHP/queryBase.php');

// classe logica
session_start();

class ClientRegister extends GetClientRegisterData
{
	public $name;
	public $email;
	public $cpf;
	public $phone;
	public $pass;
	public $cpass;


	// variavel de conexao

	public $connection;

	function __construct()
	{
		$this->name = parent::$g_name;
		$this->email = parent::$g_email;
		$this->cpf = parent::getCpf();
		$this->phone = parent::getPhone();
		$this->pass = parent::getPass();
		$this->cpass = parent::getCPass();

		$this->conx = (new Conexao())->getCon();
	}

	public function verifyRegister(){
		$query = $_SESSION['select_verify'];
		$stmt = $this->conx->prepare($query);
		$stmt->execute();

		$row = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($this->pass == $this->cpass) {
			if ($row['mail_client'] == $this->email) {
				echo "Email já existente";
			}else{
				$this->insertData();
			}
		}else{
			echo "As senhas não se conferem";
		}

		
	}

	public function insertData(){
		$query = $_SESSION['insert_query'];

		$stmt = $this->conx->prepare($query);
		$stmt->bindValue(':name_client', $this->name);
		$stmt->bindValue(':mail_client', $this->email);
		$stmt->bindValue(':cpf_client', $this->cpf);
		$stmt->bindValue(':phone_client', $this->phone);
		$stmt->bindValue(':pass_client', $this->pass);
		$stmt->execute();

		echo "Cadastro realizado com sucesso, faça login";
	}




	
}
?>