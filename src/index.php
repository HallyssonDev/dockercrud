<?php 
require_once('Classes/ClientRegister.php');
require_once('Classes/GetClientRegisterData.php');

if (isset($_POST['btn-register'])) {
    (new GetClientRegisterData()); //Recebe os dados 
    (new ClientRegister())->verifyRegister(); //Verificação
}

require_once("Classes/ClientLogin.php");
require_once("Classes/GetClientLoginData.php");

if (isset($_POST['btn-login'])) {
    (new GetClientLoginData());
    (new ClientLogin())->verifyLogin();
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Lobby</title>
    <link rel="stylesheet" type="text/css" href="css/style.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.css" />
</head>

<body>
    <header class="main-header">
        <h1>Hotel 100 Estrelas</h1>
    </header>

    <div class="main-content">

        <div class="register-side">
           <form method="POST" action="<?php $_SERVER['PHP_SELF']?>">
            <div>
                <input type="text" name="clientname" placeholder="Digite seu nome" autocomplete="off" required>
            </div>
            <div>
                <input type="text" name="clientmail" placeholder="Digite seu email" autocomplete="off" required>
            </div>
            <div>
                <input type="text" name="clientcpf" placeholder="CPF" autocomplete="off" required>
            </div>
            <div>
                <input type="text" name="clientphone" placeholder="Telefone" autocomplete="off" required>
            </div>
            <div>
                <input type="password" name="clientpass" placeholder="Senha" autocomplete="off" required>
            </div>
            <div>
                <input type="password" name="clientcpass" placeholder="Confirme sua senha" autocomplete="off" required>
            </div>
            <div>
                <button name="btn-register">Cadastrar</button>
            </div>
        </form>
    </div>

    <div class="login-side">
        <form method="POST" action="<?php $_SERVER['PHP_SELF'] ?>">
            <div>
                <input type="text" name="mailLogin" placeholder="Digite seu email">
            </div>
            <div>
                <input type="password" name="pwdLogin" placeholder="Confirme sua senha">
            </div>
            <div>
                <button name="btn-login">Login</button>
            </div>
        </form>
    </div>


</div>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/jquery@3.6.0/dist/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-modal/0.9.1/jquery.modal.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
    document.querySelector('.up-btn').addEventListener('click', function(){
        alert('asd');
    });
</script>
</body>
</html>